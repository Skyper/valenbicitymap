# Valenbisi Availability Map

Interactive map showing the number of available bikes at each
[Valenbisi][valenbisi] station in the city of Valencia, Spain.

This software relies on [Leaflet][leaflet], [OpenStreetMap][osm] and
[CityBikes][citybikes-api]. The JavaScript code is bundled with
[webpack][webpack].

## How To

### Build

To install the dependencies with [pNPM][pnpm]:

    pnpm install

To generate the distribution files:

    pnpm run build

The distribution files will be available in the `dist` folder.

### Check

To lint and apply fixes to the source code:

    pnpm run lint

To format the source code:

    pnpm run format

To do both at once:

    pnpm run check

## License

This project is [GNU GPLv3][gpl-v3] licensed.

 [citybikes-api]: https://api.citybik.es/v2/ "CityBikes API Documentation"
 [gpl-v3]: https://www.gnu.org/licenses/gpl-3.0.txt "GNU General Public License v3.0"
 [leaflet]: https://leafletjs.com/ "Leaflet"
 [osm]: https://www.openstreetmap.org "OpenStreetMap"
 [pnpm]: https://pnpm.io/ "pNPM"
 [valenbisi]: https://www.valenbisi.es/ "Valenbisi"
 [webpack]: https://webpack.js.org/ "WebPack"
