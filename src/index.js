import "leaflet.locatecontrol";
import "leaflet.locatecontrol/dist/L.Control.Locate.min.css";
import L from "leaflet";

const defaultCenter = [39.4699075, -0.3762881];
const defaultZoomLevel = 14;
const defaultLocateZoomLevel = 17;

const mapOptions = {
  center: defaultCenter,
  zoom: defaultZoomLevel,
  zoomControl: false,
};

const map = L.map("map", mapOptions).addControl(
  L.control.zoom({
    position: "bottomright",
  }),
);

L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
  maxZoom: 19,
  attribution:
    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> | <a href="https://citybik.es">CityBikes</a>',
}).addTo(map);

L.control
  .locate({
    strings: {
      title: "Locate me",
    },
    position: "bottomright",
    initialZoomLevel: defaultLocateZoomLevel,
    circleStyle: {
      interactive: false,
    },
  })
  .addTo(map);

/**
 * Fetches Valenbisi station data from CityBikes REST API and returns an
 * array of station objects.
 *
 * See https://api.citybik.es/v2/ for further details on the CityBikes REST
 * API.
 */
async function fetchStations() {
  const resp = await fetch("https://api.citybik.es/v2/networks/valenbisi");
  const rawData = await resp.json();
  const stations = rawData.network.stations;

  return stations;
}

/**
 * Plots the Valenbisi stations on the map.
 */
async function plotStations(stations) {
  for (const s of stations) {
    L.circleMarker([s.latitude, s.longitude], {
      alt: s.name,
      fill: true,
      fillColor: getStationColour(s),
      fillOpacity: 1,
    })
      .addTo(map)
      .bindPopup(`
        <h4> ${s.extra.address} </h4>
        Bikes available: ${s.free_bikes}/${s.extra.slots}
      `);
  }
}

/**
 * Returns the colour of the station marker depending on the number of
 * available bikes at the station given as parameter.
 */
function getStationColour(station) {
  if (station.free_bikes >= 8) {
    return "#009933";
  }

  if (station.free_bikes >= 4) {
    return "#ffb43f";
  }

  return "#e33033";
}

fetchStations().then((stations) => {
  plotStations(stations);
});
